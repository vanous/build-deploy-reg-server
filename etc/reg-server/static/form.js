window.addEventListener('load', function () {
	var activeMember = document.getElementById('activeMember');
	var supportingMember = document.getElementById('supportingMember');
	var privat = document.getElementById('private');
	var corporate = document.getElementById('corporate');
	var firstName = document.getElementById('firstName');
	var name = document.getElementById('name');
	var organization = document.getElementById('organization');
	var addr1 = document.getElementById('addr1');
	var zipcode = document.getElementById('zipcode');
	var city = document.getElementById('city');
	var country = document.getElementById('country');
	var email = document.getElementById('email');
	var iban = document.getElementById('iban');
	var submit = document.getElementById('submit');
	submit.disabled = true;

	document.getElementById('form').oninput = function () { validateForm(); }
	document.getElementById('form').onchange = function () { validateForm(); }
	document.getElementById('skills').oninput = function () { document.getElementById('skillsOther').checked = true; }
	document.getElementById('contributionCustom').oninput = function () { document.getElementById('customContribution').checked = true; }

	function validateForm () {
		if (isValidMembershipType()
		 && isValidEmail(email.value)
		 && isValidAddress()
		 && isValidMembershipContribution()
		 && isValidIBANNumber(iban.value))
			submit.disabled = false;
		else
			submit.disabled = true;
	}

	activeMember.onchange = function () {
		corporate.parentNode.classList.add('unavailable');
		corporate.disabled = true;
		corporate.checked = false;
	};

	supportingMember.onchange = function () {
		corporate.parentNode.classList.remove('unavailable');
		corporate.disabled = false;
	};

	corporate.onchange = function () {
		activeMember.parentNode.classList.add('unavailable');
		activeMember.disabled = true;
		activeMember.checked = false;
		firstName.style.display = 'none';
		name.style.display = 'none';
		organization.style.display = 'initial';
	};

	privat.onchange = function () {
		activeMember.parentNode.classList.remove('unavailable');
		activeMember.parentNode.style.visibility = 'visible';
		activeMember.disabled = false;
		firstName.style.display = 'initial';
		name.style.display = 'initial';
		organization.style.display = 'none';
	};

	email.oninput = function () {
		if (isValidEmail(email.value))
			email.classList.remove('error');
		else
			email.classList.add('error');
	};

	iban.oninput = function () {
		for (var i=0, j=0, s=''; i<this.value.length; i++) {
			var c = this.value[i];
			if (c != ' ') {
				s += c;
				if (++j % 4 == 0 && i < this.value.length-1)
					s += ' ';
			}
		}
		iban.value = s.toUpperCase();
		if (isValidIBANNumber(iban.value))
			iban.classList.remove('error');
		else
			iban.classList.add('error');
	};

	function isValidMembershipType () {
		if (activeMember.checked && privat.checked)
			return true;
		if (supportingMember.checked && (privat.checked || corporate.checked))
			return true;
		return false;
	}

	function isValidEmail (email) {
		var re = /^\S+@\S+$/;
		return re.test(email);
	}

	function isValidAddress () {
		if (privat.checked) {
			if (firstName.value == "")
				return false;
			if (name.value == "")
				return false;
		} else if (corporate.checked) {
			if (organization.value == "")
				return false;
		} else
			return false;
		if (addr1.value == "")
			return false;
		if (zipcode.value == "")
			return false;
		if (city.value == "")
			return false;
		if (country.value == "")
			return false;
		return true;
	}

	function isValidMembershipContribution () {
		var f = document.getElementsByName('frequency');
		var c = document.getElementsByName('contribution');
		var frequency = undefined;
		var contribution = undefined;
		for (var i = 0; i < f.length; i++) {
			if (f[i].checked) {
				frequency = parseFloat(f[i].value);
				break;
			}
		}
		for (var i = 0; i < c.length; i++) {
			if (c[i].checked) {
				if (c[i].value == 'custom') {
					var contributionCustom = document.getElementById('contributionCustom')
					contribution = parseFloat(contributionCustom.value);
					if (contribution < 10 || contribution > 1000) {
						contributionCustom.classList.add('error');
						return false;
					} else {
						contributionCustom.classList.remove('error');
					}
				} else {
					contribution = parseFloat(c[i].value);
				}
				break;
			}
		}
		if (frequency != 1 && frequency != 3 && frequency != 6 && frequency != 12)
			return false;
		var annualContribution = contribution * 12 / frequency;
		if (!isFinite(annualContribution))
			return false;
		if (activeMember.checked && annualContribution < 24 /* annual contribution for active members */)
			return false;
		if (contribution < 10 /* minimum transaction size */)
			return false;
		return true;
	}

	/* cf. https://stackoverflow.com/questions/21928083/iban-validation-check */
	function isValidIBANNumber (input) {
		/**
		 * Returns 1 if the IBAN is valid
		 * Returns any other number (checksum) when the IBAN is invalid (check digits do not match)
		 */
		function mod97 (string) {
			var checksum = string.slice(0, 2), fragment;
			for (var offset = 2; offset < string.length; offset += 7) {
				fragment = String(checksum) + string.substring(offset, offset + 7);
				checksum = parseInt(fragment, 10) % 97;
			}
			return checksum;
		}
		var CODE_LENGTHS = {
			AD: 24, AE: 23, AT: 20, AZ: 28, BA: 20, BE: 16, BG: 22, BH: 22, BR: 29,
			CH: 21, CR: 21, CY: 28, CZ: 24, DE: 22, DK: 18, DO: 28, EE: 20, ES: 24,
			FI: 18, FO: 18, FR: 27, GB: 22, GI: 23, GL: 18, GR: 27, GT: 28, HR: 21,
			HU: 28, IE: 22, IL: 23, IS: 26, IT: 27, JO: 30, KW: 30, KZ: 20, LB: 28,
			LI: 21, LT: 20, LU: 20, LV: 21, MC: 27, MD: 24, ME: 22, MK: 19, MR: 27,
			MT: 31, MU: 30, NL: 18, NO: 15, PK: 24, PL: 28, PS: 29, PT: 25, QA: 29,
			RO: 24, RS: 22, SA: 24, SE: 24, SI: 19, SK: 24, SM: 27, TN: 24, TR: 26
		};
		// keep only alphanumeric characters
		var iban = String(input).toUpperCase().replace(/[^A-Z0-9]/g, '');
		// match and capture (1) the country code, (2) the check digits, and (3) the rest
		var code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/);
		if (!code || iban.length !== CODE_LENGTHS[code[1]]) {
			return false;
		}
		var digits = (code[3] + code[1] + code[2]).replace(/[A-Z]/g, function (letter) {
			return letter.charCodeAt(0) - 55;
		});
		return mod97(digits) == 1;
	}
});

