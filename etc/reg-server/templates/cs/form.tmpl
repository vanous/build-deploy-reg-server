<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="/static/form.css">
		<script type="text/javascript" src="/static/form.js"></script>
		<link rel="icon" type="image/x-icon" href="/static/favicon.ico" />
	</head>
	<body>
		<div class="header">
			<a href="https://codeberg.org"><img src="/static/codeberg.svg" height="120" alt=""></a>
			<h1>Přidejte se ke Codeberg e.V. a aktivně podpořte vývoj Svobodného a otevřeného software!</h1>
		</div>
		<form id="form" method="post" action="/post">
			<div style="columns: 30em;">
				<fieldset>
					<legend>Členství</legend>
					<div class="error">{{.errors.membership}}</div>
					<p class="notes">
						Jako Podpůrný člen přímo podporujete záměry Codeberg e.V. a budete dostávat pravidelmé aktualizace
						o činnosti Codeberg e.V. Jako Aktivní člen se stáváte členem Codeberg e.V. a zíkáváte volební práva.
					</p>
					<div class="hbox equalwidth">
						<label><input type="radio" name="membershipType" id="supportingMember" value="supportingMember">Podpůrné členství</label>
						<label><input type="radio" name="membershipType" id="activeMember" value="activeMember">Aktivní členství</label>
					</div>
					<p class="notes">
						Soukromé osoby si mohou zvolit buďto podpůrné nebo aktivní členství. Právnické osoby mohou podpořit
						Codeberg e.V. formou podpůrného členství.
					</p>
					<div class="hbox equalwidth">
						<label><input type="radio" name="memberType" id="private" value="private">Soukromá osoba</label>
						<label><input type="radio" name="memberType" id="corporate" value="corporate">Právnická osoba</label>
					</div>
				</fieldset>
				<fieldset>
					<legend>Adresa</legend>
					<p class="notes">
						Pravidelné novinky o práci Codeberg e.V., pozvánky k volbám a ke členským schůzím
						jsou zasílány emailem. Vaše emailová adresa je také použita k identifikaci Vašeho členství a zároveň Vám
						umožňuje zrušení členství odesláním žádosti z této adresy.
					</p>
					<div class="error">{{.errors.email}}</div>
					<div class="vbox">
						<input type="text" id="email" name="email" required autocomplete="email" placeholder="E-mail">
					</div>
					<p class="notes">
						Jako Codeberg e.V. máme zákonnou povinnost udržovat aktuální záznamy o poštovních adresách členů.
						Poštovní zprávu bychom Vám zaslali pouze v případě nefunkční emailové adresy.
					</p>
					<div class="error">{{.errors.firstName}}</div>
					<div class="error">{{.errors.name}}</div>
					<div class="error">{{.errors.addr1}}</div>
					<div class="error">{{.errors.addr2}}</div>
					<div class="error">{{.errors.zipcode}}</div>
					<div class="error">{{.errors.city}}</div>
					<div class="error">{{.errors.country}}</div>
					<div class="vbox">
						<div class="hbox">
							<input type="text" id="firstName" name="firstName" autocomplete="given-name" placeholder="Křestní Jméno">
							<input type="text" id="name" name="name" autocomplete="family-name" placeholder="Příjmení">
							<input type="text" id="organization" name="organization" autocomplete="organization" placeholder="Organizace">
						</div>
						<input type="text" id="addr1" name="addr1" required autocomplete="address-line1" placeholder="Ulice a číslo">
						<input type="text" id="addr2" name="addr2" autocomplete="address-line2" placeholder="Doplněk adresy">
						<div class="hbox">
							<input type="text" id="zipcode" name="zipcode" size="7" required autocomplete="postal-code" placeholder="PSČ">
							<input type="text" id="city" name="city" required autocomplete="city" placeholder="Obec">
							<input type="text" id="country" name="country" size="15" required autocomplete="country-name" placeholder="Země">
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Nehmotná podpora</legend>
					<p class="notes">
						Chcete-li podpořit projekt ve kterékoliv z následujících oblastí, zaškrtněte prosím položku/y níže.
					</p>
					<div class="columns">
						<label><input type="checkbox" name="skillsAppDev" value="1"><span class="wrap">Vývoj aplikací</span></label>
						<label><input type="checkbox" name="skillsSecurity" value="1"><span class="wrap">IT bezpečnost a penetrační testy</span></label>
						<label><input type="checkbox" name="skillsDB" value="1"><span class="wrap">Databázové replikace</span></label>
						<label><input type="checkbox" name="skillsFS" value="1"><span class="wrap">Distibuované souborové systémy, replikace</span></label>
						<label><input type="checkbox" name="skillsTax" value="1"><span class="wrap">Auditing, Finance and Daňové zákony</span></label>
						<label><input type="checkbox" name="skillsLegal" value="1"><span class="wrap">IT-Zákony, Práva asociací, pro bono právní poradenství</span></label>
						<label><input type="checkbox" name="skillsPR" value="1"><span class="wrap">Spolupráce a PR marketing, obzvláště pro Svobodný a Otevřený software</span></label>
						<label><input type="checkbox" name="skillsFundraising" value="1"><span class="wrap">Fund-raising, získávání finančních prostředků</span></label>
						<label><input type="checkbox" id="skillsOther" name="skillsOther" value="1"><input type="text" id="skills" name="skills" placeholder=""></label>
					</div>
				</fieldset>
				<fieldset>
					<legend>Poplatky</legend>
					<p class="notes">
						Členské poplatky jsou vybírány formou SEPA debitu, je-li to možné. Kdy by měla transakce proběhnout
                        a kolik si přejete přispívat??
					</p>
					<div class="error">{{.errors.frequency}}</div>
					<div style="columns: 10em 4;">
						<label><input type="radio" name="frequency" value="1">Měsíčně</label>
						<label><input type="radio" name="frequency" value="3">Čtvrtletně</label>
						<label><input type="radio" name="frequency" value="6">Půlročně</label>
						<label><input type="radio" name="frequency" value="12">Ročně</label>
					</div>
					<p class="notes">
						Jak specifikováno v našich stanovách, velikost členského příspěvku si můžete zvolit sami.
                        Banka vyžaduje minimální transakci v částce 10€, pro pokrytí poplatku za transakci. 
						Minimální roční příspěvek pro aktivní členství a volební práva je 24€.
					</p>
					<div class="error">{{.errors.contribution}}</div>
					<div style="columns: 5em 3">
						<label><input type="radio" name="contribution" value="10">10€</label>
						<label><input type="radio" name="contribution" value="15">15€</label>
						<label><input type="radio" name="contribution" value="25">25€</label>
						<label><input type="radio" name="contribution" value="50">50€</label>
						<label><input type="radio" name="contribution" value="100">100€</label>
						<label><input type="radio" name="contribution" value="150">150€</label>
						<label><input type="radio" name="contribution" value="250">250€</label>
						<label><input type="radio" name="contribution" value="custom" id="customContribution"><input type="text" name="contributionCustom" id="contributionCustom" size="5" placeholder="">€</label>
					</div>
					<p class="notes">
						Členské příspěvky jsou vybírány formou SEPA debitu. Prosím pošlete dotaz na
						<a href="mailto:contact@codeberg.org">contact@codeberg.org</a> pro jiné platební metody.
					</p>
					<div class="error">{{.errors.iban}}</div>
					<div class="hbox">
						<input type="text" id="iban" name="iban" required autocomplete="off" placeholder="IBAN">
						<input type="text" id="bic" name="bic" size="11" autocomplete="off" placeholder="BIC">
					</div>
				</fieldset>
				<fieldset>
					<p style="font-size: small;">
					Podpisem tohoto formuláře autorizujete: (A) Codeberg e.V. (DE40ZZZ00002172825) pro zaslání instrukcí do Vaší banky
                    pro debetování Vašeho účtu a (B) Vaší banku pro tuto transakci v souvislosti s instrukcemi z Codeberg e.V. 
                    Jako součást Vašich práv máte nárok na vrácení peněz Vaší bankou dle obchodních podmínek viz Vaše smlouva s 
                    bankou. O vrácení peněz musíte požádat do osmi (8) týdnů od počátku transakce.
					</p>
					<button type="submit" id="submit">Přidat se nyní</button>
				</fieldset>
			</div>
		</form>
	</body>
</html>
