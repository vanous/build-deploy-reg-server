<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="/static/form.css">
		<script type="text/javascript" src="/static/form.js"></script>
		<link rel="icon" type="image/x-icon" href="/static/favicon.ico" />
	</head>
	<body>
		<div class="hbox">
			<a href="https://codeberg.org"><img src="/static/codeberg.svg" height="120" alt=""></a>
			<h1>Mitglied im Codeberg e.V. werden und die Entwicklung Freier Software fördern!</h1>
		</div>
		<form id="form" method="post" action="/post">
			<div style="columns: 30em;">
				<fieldset>
					<legend>Mitgliedschaft</legend>
					<div class="error">{{.errors.membership}}</div>
					<p class="notes">
						Als Fördermitglied können Sie die Arbeit des Codeberg e.V. direkt und effizient unterstützen. Sie erhalten regelmäßige Informationen über die Arbeit des Vereins. Mit einer aktiven Mitgliedschaft bringen Sie sich aktiv im Verein ein, und haben ein Stimmrecht in der Mitgliederversammlung.
					</p>
					<div class="hbox equalwidth">
						<label><input type="radio" name="membershipType" id="supportingMember" value="supportingMember">Fördermitgliedschaft</label>
						<label><input type="radio" name="membershipType" id="activeMember" value="activeMember">Aktive Mitgliedschaft</label>
					</div>
					<p class="notes">
						Privatpersonen können Fördermitglied oder aktives Mitglied sein.
						Firmen können den Codeberg e.V. mit einer Fördermitgliedschaft unterstützen.
					</p>
					<div class="hbox equalwidth">
						<label><input type="radio" name="memberType" id="private" value="private">Privatperson</label>
						<label><input type="radio" name="memberType" id="corporate" value="corporate">Verein oder Firma</label>
					</div>
				</fieldset>
				<fieldset>
					<legend>Anschrift</legend>
					<p class="notes">
						Per E-Mail werden regelmäßig Informationen über die Arbeit des Vereins versendet, 
						und etwa Termine zu Abstimmungen und Mitgliederversammlungen mitgeteilt.
						Auch ist die Mitgliedschaft jederzeit per E-Mail formlos kündbar.
					</p>
					<div class="error">{{.errors.email}}</div>
					<div class="vbox">
						<input type="text" id="email" name="email" required autocomplete="email" placeholder="E-Mail">
					</div>
					<p class="notes">
						Als Verein sind wir rechtlich dazu verpflichtet, die aktuelle Postanschrift unserer Mitglieder 
						zu kennen. Normalerweise versenden wir keine Post. Nur in Ausnahmefällen wie einer nicht funktionierenden
						E-Mail-Adresse werden wir per Brief Kontakt aufnehmen.
					</p>
					<div class="error">{{.errors.firstName}}</div>
					<div class="error">{{.errors.name}}</div>
					<div class="error">{{.errors.addr1}}</div>
					<div class="error">{{.errors.addr2}}</div>
					<div class="error">{{.errors.zipcode}}</div>
					<div class="error">{{.errors.city}}</div>
					<div class="error">{{.errors.country}}</div>
					<div class="vbox">
						<div class="hbox">
							<input type="text" id="firstName" name="firstName" autocomplete="given-name" placeholder="Vorname">
							<input type="text" id="name" name="name" autocomplete="family-name" placeholder="Name">
							<input type="text" id="organization" name="organization" autocomplete="organization" placeholder="Verein oder Firma">
						</div>
						<input type="text" id="addr1" name="addr1" required autocomplete="address-line1" placeholder="Straße und Hausnummer">
						<input type="text" id="addr2" name="addr2" autocomplete="address-line2" placeholder="Addresszusatz">
						<div class="hbox">
							<input type="text" id="zipcode" name="zipcode" size="7" required autocomplete="postal-code" placeholder="Postleitzahl">
							<input type="text" id="city" name="city" required autocomplete="city" placeholder="Stadt">
							<input type="text" id="country" name="country" size="15" required autocomplete="country-name" placeholder="Land">
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Immaterielle Unterstützung</legend>
					<p class="notes">
						Wenn Sie sich aktiv in die Arbeit des Codeberg e.V. einbringen möchten,
						und sich vorstellen können,
						bei Fragen und Problemen beratend zur Verfügung zu stehen,
						lassen Sie uns das bitte wissen!
					</p>
					<div class="columns">
						<label><input type="checkbox" name="skillsAppDev" value="1"><span class="wrap">Application Development</span></label>
						<label><input type="checkbox" name="skillsSecurity" value="1"><span class="wrap">IT Security und Penetration Tests</span></label>
						<label><input type="checkbox" name="skillsDB" value="1"><span class="wrap">Datenbanken, Replikation und Performance</span></label>
						<label><input type="checkbox" name="skillsFS" value="1"><span class="wrap">Distributed File Systems, Replikation und Performance</span></label>
						<label><input type="checkbox" name="skillsTax" value="1"><span class="wrap">Kassenprüfung, Finanzen und Steuerrecht</span></label>
						<label><input type="checkbox" name="skillsLegal" value="1"><span class="wrap">IT-Recht, Vereinsrecht, pro bono Rechtsberatung</span></label>
						<label><input type="checkbox" name="skillsPR" value="1"><span class="wrap">Networking und Öffentlichkeitsarbeit im Bereich Open-Source</span></label>
						<label><input type="checkbox" name="skillsFundraising" value="1"><span class="wrap">Fundraising</span></label>
						<label><input type="checkbox" id="skillsOther" name="skillsOther" value="1"><input type="text" id="skills" name="skills" placeholder=""></label>
					</div>
				</fieldset>
				<fieldset>
					<legend>Beitrag</legend>
					<p class="notes">
						Der Mitgliedsbeitrag wird per SEPA-Lastschrift abgebucht. Wann soll
						dieser Betrag gebucht werden, und welchen Mitgliedsbeitrag möchten Sie
						beitragen?
					</p>
					<div class="error">{{.errors.frequency}}</div>
					<div style="columns: 10em 4;">
						<label><input type="radio" name="frequency" value="1">Monatlich</label>
						<label><input type="radio" name="frequency" value="3">Quartalsweise</label>
						<label><input type="radio" name="frequency" value="6">Halbjährlich</label>
						<label><input type="radio" name="frequency" value="12">Jährlich</label>
					</div>
					<p class="notes">
						Jedes Mitglied im Codeberg e.V. kann seinen Mitgliedsbeitrag frei wählen. 
						Für eine aktive Mitgliedschaft mit Stimmrecht wurde von der Mitgliederversammlung
						ein Mindest-Jahresbeitrag von 24€ beschlossen.
						Für SEPA-Lastschriften hat unsere Bank eine Mindest-Transaktionsgröße von 10€ festgelegt.
					</p>
					<div class="error">{{.errors.contribution}}</div>
					<div style="columns: 5em 3">
						<label><input type="radio" name="contribution" value="10">10€</label>
						<label><input type="radio" name="contribution" value="15">15€</label>
						<label><input type="radio" name="contribution" value="25">25€</label>
						<label><input type="radio" name="contribution" value="50">50€</label>
						<label><input type="radio" name="contribution" value="100">100€</label>
						<label><input type="radio" name="contribution" value="150">150€</label>
						<label><input type="radio" name="contribution" value="250">250€</label>
						<label><input type="radio" name="contribution" value="custom" id="customContribution"><input type="text" name="contributionCustom" id="contributionCustom" size="5" placeholder="">€</label>
					</div>
					<p class="notes">
						Für andere Buchungsverfahren und Beiträge bitte E-Mail an <a href="mailto:contact@codeberg.org">contact@codeberg.org</a>.
					</p>
					<div class="error">{{.errors.iban}}</div>
					<div class="hbox">
						<input type="text" id="iban" name="iban" required autocomplete="off" placeholder="IBAN">
						<input type="text" id="bic" name="bic" size="11" autocomplete="off" placeholder="BIC">
					</div>
					<p style="font-size: small;">Ich ermächtige den Codeberg e.V. (DE40ZZZ00002172825), Zahlungen von meinem Konto mittels Lastschrift einzuziehen. Zugleich weise ich mein Kreditinstitut an, die vom Codeberg e.V. auf mein Konto gezogenen Lastschriften einzulösen. Ich kann innerhalb von acht Wochen, beginnend mit dem Belastungsdatum, die Erstattung des belasteten Betrages verlangen. Es gelten dabei die mit meinem Kreditinstitut vereinbarten Bedingungen.</p>
				</fieldset>
				<fieldset>
					<button type="submit" id="submit">Jetzt Mitglied werden</button>
				</fieldset>
			</div>
		</form>
	</body>
</html>
