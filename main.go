package main

import (
	"os"
	"os/exec"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"html/template"
	"strings"
	"regexp"
	"path"
	"strconv"
	"io"
	"io/ioutil"
	"time"
)

func (serverCtx *serverContext) getClientIP() string {
	hdr := serverCtx.req.Header["X-Forwarded-For"]
	if hdr == nil {
		return serverCtx.req.RemoteAddr
	}
	return strings.Join(hdr, ";")
}

func (serverCtx *serverContext) logLine(msg string) {
	fmt.Println(fmt.Sprintf("%s: %s", serverCtx.req.RemoteAddr, msg))
}

func (serverCtx *serverContext) serverError(msg string) {
	serverCtx.logLine(fmt.Sprintf("ERROR: %s", msg))
	http.Error(serverCtx.writer, "Internal Server Error", http.StatusInternalServerError)
}

// Very crude parser of Accept-Language header. There is a package on
// golang.org that is supposed to do this properly. But this will do for now--
// let's stick to the standard library here if we can.
func chooseLang(req *http.Request) string {
	const def = "en"
	hdr := req.Header["Accept-Language"]
	if hdr == nil { return def }
	langs := strings.SplitN(hdr[0], ";", 2)
	if langs == nil { return def }
	lang_str := langs[0]
	for _, lang := range strings.Split(lang_str, ",") {
		if strings.HasPrefix(lang, "de") {
			return "de"
		}
		if strings.HasPrefix(lang, "en") {
			return "en"
		}
		if strings.HasPrefix(lang, "it") {
			return "it"
		}
		if strings.HasPrefix(lang, "nl") {
			return "nl"
		}
		if strings.HasPrefix(lang, "cs") {
			return "cs"
		}
	}
	return def
}

type serverContext struct {
	writer http.ResponseWriter
	req *http.Request
	lang string
}

// For passing around to all our handlers and things
func newServerContext(writer http.ResponseWriter, req *http.Request) *serverContext {
	return &serverContext{writer, req, chooseLang(req)}
}

// For rendering variables in templates
type renderingContext map[string] interface {}

type handler struct {
	// If method and path match, we call handler
	method	string
	path	string
	handler	func(*serverContext, renderingContext)
}

// This is all our little application does
var handlers = [...] handler {
	{"POST", "/post", handlePost},
	{"GET", "/thanks", renderThanks},
	{"GET", "/static", renderStatic},
	{"GET", "/", renderForm},
}

type validator struct {
	required bool
	validator func (lang string, datum string) error
}

var validators = map[string]validator {
	"email": {true, checkEmail},
	"addr1": {true, nil},
	"zipcode": {true, nil},
	"city": {true, nil},
	"country": {true, nil},
	"iban": {true, nil},
}

const (
	REQUIRED_FIELD = iota
	BAD_EMAIL = iota
)

var errorMessages = map[string]map[int]string {
	"de": {
		REQUIRED_FIELD: "Dieses Feld darf nicht leer sein",
		BAD_EMAIL: "Bitte eine gültige E-Mail Adresse eingeben",
	},
	"en": {
		REQUIRED_FIELD: "This field is required",
		BAD_EMAIL: "Please enter a valid email address",
	},
	"it": {
		REQUIRED_FIELD: "Questo campo è obbligatorio",
		BAD_EMAIL: "Per favore inserisci un indirizzo email valido.",
	},
	"nl": {
		REQUIRED_FIELD: "Dit veld is verplicht",
		BAD_EMAIL: "Voer een geldig email adres in",
	},
	"cs": {
		REQUIRED_FIELD: "Toto pole je vyžadováno",
		BAD_EMAIL: "Vyplňte prosím platnou emailovou adresu",
	},
}

func errorMessage(lang string, code int) error {
	return fmt.Errorf(errorMessages[lang][code])
}

// Checks datum and returns an error
func (this *validator) checkField(serverCtx *serverContext, datum string) error {
	lang := serverCtx.lang
	if this.required && datum == "" {
		return errorMessage(lang, REQUIRED_FIELD)
	}
	if this.validator != nil {
		err := this.validator(lang, datum)
		if err != nil { return err }
	}
	return nil
}

func checkEmail(lang string, email string) error {
	//emailPat := regexp.MustCompile("^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$")
	//emailPat := regexp.MustCompile("^[\w\d\.]+@[\w\d\.]+")
	//emailPat := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	//emailPat := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	emailPat := regexp.MustCompile("^\\S+@\\S+$")
	if !emailPat.MatchString(email) {
		return errorMessage(lang, BAD_EMAIL)
	}
	return nil
}

func renderTemplate(serverCtx *serverContext, fname string, renderCtx renderingContext) {
	writer, lang := serverCtx.writer, serverCtx.lang
	templ := template.New(fmt.Sprintf("%s", fname))
	templ.ParseFiles(fmt.Sprintf("templates/%s/%s", lang, fname))
	templ.Execute(writer, renderCtx)
}

func renderForm(serverCtx *serverContext, renderCtx renderingContext) {
	serverCtx.logLine("Loaded form")
	renderTemplate(serverCtx, "form.tmpl", renderCtx)
}

// Convert a "money" string (i.e. with separators for thousands and decimals)
// to a float
func parseMoney(strAmount string) (float64, error) {
	pat := regexp.MustCompile("[,'.]")
	fields := pat.Split(strAmount, -1)

	n := len(fields)
	if n == 0 {
		return 0, fmt.Errorf("Bad Money Amount")
	}
	if n == 1 {
		return strconv.ParseFloat(strAmount, 64)
	}

	var fraction string
	if len(fields[n-1]) == 2 {
		fraction = fields[n-1]
	} else {
		fraction = "00"
	}

	var reformatted string
	for _, f := range fields[:n-1] {
		reformatted += f
	}
	reformatted = fmt.Sprintf("%s.%s", reformatted, fraction)
	return strconv.ParseFloat(reformatted, 64)
}

// Given the post data return a flattened and cleaned-up version of it and any errors.
func validate(serverCtx *serverContext, postData url.Values) (map[string]string, map[string]string) {
	cleanedData := make(map[string]string)
	errors := make(map[string]string)
	for k, v := range postData {
		if len(v) != 1 { continue }
		if validator, exists := validators[k]; exists {
			err := validator.checkField(serverCtx, v[0])
			if err != nil {
				errors[k] = k + " : " + err.Error()
				continue
			}
		}
		cleanedData[k] = v[0]
	}
	frequency, err := strconv.ParseFloat(cleanedData["frequency"], 64)
	if err != nil || (frequency != 1 && frequency != 3 && frequency != 6 && frequency != 12) {
		errors["frequency"] = "Form error"
	}
	contributionString := cleanedData["contribution"]
	if contributionString == "custom" {
		contributionString = cleanedData["contributionCustom"]
	}
	activeMembership := cleanedData["membershipType"] == "activeMember"
	contribution, err := parseMoney(contributionString)
	annualContribution := contribution * 12 / frequency
	if err != nil || contribution < 10 || (activeMembership && annualContribution < 24) {
		errors["contribution"] = fmt.Sprintf("Form error : contribution %g (%g p.a.)", contribution, annualContribution)
		//serverCtx.logLine(fmt.Sprintf("Form error : cleanedData['membershipType'] '%s'", cleanedData["membershipType"]))
		//serverCtx.logLine(fmt.Sprintf("Form error : cleanedData['contribution'] '%s'", contributionString))
		//serverCtx.logLine(fmt.Sprintf("Form error : cleanedData['frequency'] '%s'", cleanedData["frequency"]))
	}
	cleanedData["timestamp"] = time.Now().String()
	cleanedData["registrationClientIP"] = serverCtx.getClientIP()
	return cleanedData, errors
}

func startRegistration(serverCtx *serverContext, json []byte) error {
	p := "gpg --no-default-keyring --keyring /etc/reg-server/public-key.asc.gpg --trust-model always --encrypt --armor --recipient registration@codeberg.org | mail -a 'Content-Type: text/plain; charset=UTF-8' -s 'User registration' registration@codeberg.org"
	serverCtx.logLine(fmt.Sprintf("Executing '%s'", p))
	cmd := exec.Command("bash", "-c", p)
	stdin, _ := cmd.StdinPipe()
	io.WriteString(stdin, string(json))
	stdin.Close()
	output, err := cmd.CombinedOutput()
	if err != nil || len(output) != 0 {
		serverCtx.logLine(fmt.Sprintf("Command output '%s'", string(output)))
	}
	return err
}

func sendRegistrationEmail(serverCtx *serverContext, formData map[string]string) error {
	msg := "Hello "
	if formData["memberType"] == "corporate" {
		msg += formData["organization"] + "!\n\n"
	} else {
		msg += formData["firstName"] + "!\n\n"
	}
	msg += "We are happy to welcome you as a member of Codeberg e.V.\n"
	msg += "The following record will be stored in the membership database:\n\n"
	msg += "    {\n"
	for key, value := range formData {
		comment := ""
		if value != "" {
			if key == "iban" {
				s := []rune(value)
				for i := 4; i < len(s) - 5; i++ {
					s[i] = '*'
				}
				value = string(s)
				comment = "/* hidden here for privacy reasons */"
			}
			if key == "frequency" {
				comment = "/* " + formData["contribution"] + " EUR contribution every "  + value + " month(s) */"
			}
			if key == "registrationClientIP" {
				comment = "/* client IP address at registration. Used to detect and block abuse of online registration system */"
			}
			msg += "        \"" + key + "\" : \"" + value + "\",  " + comment + "\n"
		}
	}
	msg += "    }\n\n"
	msg += "Please review this data and let us know if anything went wrong.\n\n"
	msg += "Codeberg e.V.\n\n"
	p := "mail -a 'Content-Type: text/plain; charset=UTF-8' -s 'Welcome to Codeberg' " + formData["email"]
	cmd := exec.Command("bash", "-c", p)
	stdin, _ := cmd.StdinPipe()
	io.WriteString(stdin, string(msg))
	stdin.Close()
	output, err := cmd.CombinedOutput()
	if err != nil || len(output) != 0 {
		serverCtx.logLine(fmt.Sprintf("Command output '%s'", string(output)))
	}
	return err
}

func handlePost(serverCtx *serverContext, renderCtx renderingContext) {
	serverCtx.logLine("handlePost")
	writer, req := serverCtx.writer, serverCtx.req
	err := req.ParseForm()
	if err != nil {
		serverCtx.serverError("Can't parse form")
		return
	}
	formData, errors := validate(serverCtx, req.PostForm)
	if len(errors) != 0 {
		renderCtx = make(renderingContext)
		renderCtx["errors"] = errors
		renderForm(serverCtx, renderCtx)
		serverCtx.logLine("Form returned with errors")
		return
	}
	jsonString, err := json.Marshal(formData)
	if err != nil {
		serverCtx.serverError( "Bad JSON")
		return
	}
	err = startRegistration(serverCtx, jsonString)
	if err != nil {
		serverCtx.serverError(fmt.Sprintf("startRegistration failed: %s", err))
	} else {
		err = sendRegistrationEmail(serverCtx, formData)
		if err != nil {
			serverCtx.serverError(fmt.Sprintf("sendRegistrationEmail failed: %s", err))
		} else {
			http.Redirect(writer, req, "/thanks", http.StatusFound)
			serverCtx.logLine(fmt.Sprintf("Successful registration from <%s>", formData["email"]))
		}
	}
}

func renderThanks(serverCtx *serverContext, renderCtx renderingContext) {
	serverCtx.logLine("renderThanks")
	renderTemplate(serverCtx, "thanks.tmpl", renderCtx)
}

func getContentType(ext string) string {
	contentTypes := map[string]string {
		".css": "text/css",
		".html" : "text/html",
		".jpeg" : "image/jpeg",
		".js": "text/javascript",
		".png" : "image/png",
		".svg" : "image/svg+xml",
	}
	ret, exists := contentTypes[ext]
	if exists { return ret }
	return "text/plain"
}

func renderStatic (serverCtx *serverContext, renderCtx renderingContext) {
	writer, req := serverCtx.writer, serverCtx.req
	serverCtx.logLine("renderStatic req.URL.Path")
	basename := path.Base(req.URL.Path)
	ext := path.Ext(req.URL.Path)
	contentType := fmt.Sprintf("%s; charset=utf-8", getContentType(ext))
	staticPath := fmt.Sprintf("static/%s", basename)
	_, err := os.Stat(staticPath)
	if err != nil {
		http.NotFound(writer, req)
		return
	}
	data, err := ioutil.ReadFile(staticPath)
	if err != nil {
		serverCtx.serverError(fmt.Sprintf("Can't read static file <%s>", staticPath))
		return
	}
	writer.Header().Set("Content-Type", contentType)
	writer.Write(data)
}

func dispatcher(writer http.ResponseWriter, req *http.Request) {
	serverCtx := newServerContext(writer, req)
	defer func() {
		if r := recover(); r != nil {
			serverCtx.serverError("Unhandled exception")
			return
		}
	}()
	for i, _ := range handlers {
		handler := &handlers[i]
		if handler.method == req.Method &&
			strings.HasPrefix(req.URL.Path, handler.path) {
			handler.handler(serverCtx, nil)
			return
		}
	}
	http.NotFound(writer, req)
}

// Command-line flags, used to specify where the output goes
var (
	remoteUser, remoteHost, remotePort string
	remoteCmd, privateKeyFile string
)

func main() {
	fmt.Println("Listening on port 5000")
	http.HandleFunc("/", dispatcher)
	http.ListenAndServe(":5000", nil)
}

